<?php
ob_start();
if(!isset($_SESSION) ) session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="#">
    <title>::CMPI:: LIBRARY</title>
    <!-- Bootstrap core CSS -->
    <link href="../resource/css/style.css" rel="stylesheet" type="text/css">
    <link href="../resource/css/bootstrap.min.css" rel="stylesheet">
    <!-- Bootstrap Dropdown Hover CSS -->
    <link href="../resource/css/animate.min.css" rel="stylesheet">
    <link href="../resource/css/bootstrap-dropdownhover.min.css" rel="stylesheet">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="../resource/css/ie10-viewport-bug-workaround.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="navbar-fixed-top.css" rel="stylesheet">
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../resource/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../resource/js/ie-emulation-modes-warning.js"></script>
    <!-- Include Date Range Picker -->
    <script type="text/javascript" src="../resource/bootstrap/js/daterangepicker.js"></script>
    <link rel="stylesheet" type="text/css" href="../resource/bootstrap/css/daterangepicker.css" />

     <link href='../resource/select2/dist/css/select2.min.css' rel='stylesheet' type='text/css'>
    <!-- Required for Datepicker ended -->
    <!-- required for search, block3 of 5 start -->
    <link rel="stylesheet" href="../resource/bootstrap/css/jquery-ui.css">
    <script src="../resource/bootstrap/js/jquery.js"></script>
    <script src="../resource/bootstrap/js/jquery-ui.js"></script>
    <!--   <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
        <!-- required for search, block3 of 5 end -->
    <script src="../resource/js/index.js"></script>

</head>
<body id="body">
<div class=" headSection">
    <div class="row">
        <div class="col-sm-1"> </div>
        <div class="col-sm-2"><img src="../resource/img/cmpi.png" width="150px"> </div>
        <div class="col-sm-6 text-left"><a href="index.php"></a></div>
        <div class="col-sm-2 text-right">
            Admin Panel<p>Hi ! <b><?php // echo "<b>$singleUser->first_name $singleUser->last_name</b>"?>!</b> &nbsp; <a href="../User/Authentication/logout.php">[ Log Out ]</a></p>
        </div>
        <div class="col-sm-1"> </div>
     </div>
    <div class="container">
        <div class="row">
			<div class="col-sm-12">
			<nav  class="navbar navbar-default navbar-static">
				<div style="padding-left:0px; padding-right:0px;" class="">
					<div style="padding-top:0px; margin:0px;" class="">
					</div>
					<div style="padding-top:0px; margin-top:0px;" class="navbar-header">
						<button  type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<!--<a class="navbar-brand" href="#myPage"><h4>BSBL</h4></a>-->
					</div>
					<div style="padding-top:0px; margin-top:0px;"  class="collapse navbar-collapse" id="myNavbar">
						<div style="padding-left:0px; padding-right:0px;" class="">
							<ul class="nav navbar-nav navbar-left">
								<li class="backg"><a href="index.php">HOME</a></li>




								<li class="dropdown">
									<a id="menu" href="" class="dropdown-toggle" data-toggle="dropdown" role="button" data-hover="dropdown" aria-haspopup="true" aria-expanded="false" data-animations="zoomIn zoomIn zoomIn zoomIn">Staff <span class="caret"></span></a>
									<ul class="dropdown-menu">
										<li class="backg"><a id="menu" href="add.php?id=staff">Add new Staff</a></li>
										<li class="backg"><a id="menu" href="add.php?id=student">Add new Student</a></li>
										<li class="backg"><a id="menu" href="add.php?id=dept">Add  Department</a></li>
										<li class="backg"><a id="menu" href="add.php?id=author">Add Author</a></li>
										<li class="backg"><a id="menu" href="add.php?id=subject">Add Subject</a></li>
										<li class="backg"><a id="menu" href="add.php?id=news">Add news</a></li>
										<li class="divider"></li>
										<li class="backg"><a id="menu" href="add.php?id=book">Add book</a></li>
									   <li class="backg"><a id="menu" href="add.php?id=issuebook">Issue Book</a></li>
									</ul>


								</li>


								<li class="dropdown">
									<a id="menu" href="" class="dropdown-toggle" data-toggle="dropdown" role="button" data-hover="dropdown" aria-haspopup="true" aria-expanded="false" data-animations="zoomIn zoomIn zoomIn zoomIn">Report <span class="caret"></span></a>
									<ul class="dropdown-menu">
										<li class="backg"><a id="menu" href="view.php?id=staff">View Staff</a></li>
										<li class="backg"><a id="menu" href="view.php?id=student">View Student</a></li>
										<li class="backg"><a id="menu" href="view.php?id=department">View Department</a></li>
										<li class="backg"><a id="menu" href="view.php?id=author">View Author</a></li>
										<li class="dropdown backg "><a id="menu" href="view.php?id=subject" >View Subject</a> </li>
										<li class="backg"><a id="menu" href="view.php?id=news">View News</a></li>
										<li class="divider"></li>
										<li class="backg"><a id="menu" href="view.php?id=book">View Book</a></li>
										<li class="backg"><a id="menu" href="view.php?id=issuebook"> View Issue Book</a></li>
									</ul>

								<li class="backg"><a href="whywe.php">Why We</a></li>
								<li class="backg"><a href="contactus.php">Contact Us</a></li>
							</ul>
						</div>
					</div>
				</div>
			</nav>
			</div>
		</div>
    </div>
</div>

 <div class="row">
        <div class="col-sm-1"></div>
        <div class="col-sm-10">
            <?php echo "<div style='height: 30px; text-align: center'> <div class='alert-success' id='message'></div> </div>"; ?>
        </div>
        <div class="col-sm-1"></div>
    </div> 
