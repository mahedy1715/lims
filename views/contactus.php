<?php
include 'header.php';
?>

<div class="container">
   <div class="row">
       <div class="col-md-8"><h1>Contact Us</h1>
           <div class="col-md-4"><h3>Chairman :</h3>
              <br> Mohammad Reaz Uddin Khan
               <br> C/P : +880 1711117801
               <br> +880 1789398756
               <br> Direct Phone: +88031 726671
               <br> E-mail : reaz@inchcapebd.com
               <br>reaz@coscolbd.com</div>
           <div class="col-md-4"><h3>Managing Director :</h3>
               <br> Mohammad Shazzadur Rahman
               <br> C/P : +880 1755627550
               <br> +880 1842815159
               <br> Direct Phone: +88031 726672
               <br> E-mail: shazzad@inchcapebd.com
               <br> shazzad@coscolbd.com </div>
           <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3690.7225894224034!2d91.81422471495517!3d22.326328935310126!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30acd8b4893d82bd%3A0x56fcc103d6b0a519!2sAziz+Court%2C+88-89+Commerce+College+Road%2C+Chittagong!5e0!3m2!1sen!2sbd!4v1511584187729" style="border:0" allowfullscreen="" width="100%" height="340" frameborder="0"></iframe>

       </div>


       <div class="col-md-4"><h3>
               <br>Inchcape Shipping Lines Ltd.
               <br> Coscol Shipping Lines Ltd.</h3>
           <br> Aziz Court (18 th Floor, South Side)
           <br> 88-89, Agrabad C/A,
           <br> Chittagong - 4100, Bangladesh.
           <br> Phone : +88031 726673-5
           <br> Fax : +88-031-2851753
           <br> E-mail : ops@inchcapebd.com
           <br> Web : www.inchcapebd.com
           <br> Email : www.coscolbd.com


           <br>
           <html>
           <head>
               <script type='text/javascript'>
                   function formValidator(){ // Make quick references to our fields
                       var firstname = document.getElementById('firstname');
                       var addr = document.getElementById('addr');
                       var zip = document.getElementById('zip');
                       var state = document.getElementById('state');
                       var username = document.getElementById('username');
                       var email = document.getElementById('email'); // Check each input in the order that it appears in the form!
                       if(isAlphabet(firstname, "Please enter only letters for your name")){
                           if(isAlphanumeric(addr, "Numbers and Letters Only for Address")){
                               if(isNumeric(zip, "Please enter a valid zip code")){
                                   if(madeSelection(state, "Please Choose a State")){
                                       if(lengthRestriction(username, 6, 8)){
                                           if(emailValidator(email, "Please enter a valid email address")){
                                               return true;
                                           }
                                       }
                                   }
                               }
                           }
                       }
                       return false;
                   }
                   function notEmpty(elem, helperMsg){
                       if(elem.value.length == 0){
                           alert(helperMsg);
                           elem.focus(); // set the focus to this input
                           return false;
                       }
                       return true;
                   }
                   function isNumeric(elem, helperMsg){
                       var numericExpression = /^[0-9]+$/;
                       if(elem.value.match(numericExpression)){
                           return true;
                       }
                       else{
                           alert(helperMsg);
                           elem.focus();
                           return false;
                       }
                   }
                   function isAlphabet(elem, helperMsg){
                       var alphaExp = /^[a-zA-Z]+$/;
                       if(elem.value.match(alphaExp)){
                           return true;
                       }
                       else{
                           alert(helperMsg);
                           elem.focus();
                           return false;
                       }
                   }
                   function isAlphanumeric(elem, helperMsg){
                       var alphaExp = /^[0-9a-zA-Z]+$/;
                       if(elem.value.match(alphaExp)){
                           return true;
                       }
                       else{
                           alert(helperMsg);
                           elem.focus();
                           return false;
                       }
                   }
                   function lengthRestriction(elem, min, max){
                       var uInput = elem.value;
                       if(uInput.length >= min && uInput.length <= max){
                           return true;
                       }
                       else{
                           alert("Please enter between " +min+ " and " +max+ " characters");
                           elem.focus();
                           return false;
                       }
                   }
                   function madeSelection(elem, helperMsg){
                       if(elem.value == "Please Choose"){
                           alert(helperMsg);
                           elem.focus();
                           return false;
                       }
                       else{
                           return true;
                       }
                   }
                   function emailValidator(elem, helperMsg){
                       var emailExp = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
                       if(elem.value.match(emailExp)){
                           return true;
                       }
                       else{
                           alert(helperMsg);
                           elem.focus();
                           return false;
                       }
                   }
               </script>
           </head>
           <body>
           <form onsubmit='return formValidator()' >
               <h4>Name:</h4> <input class="form-control" type='text' id='firstname' required /><br />


              <h4>Email:</h4> <input class="form-control" type='text' id='email' required /><br />
               <input type='submit' value='submit' />
           </form>
           </body>
           </html>
       </div>



   </div>
</div>


<?php
include 'footer.php';
include 'footer_script.php';
?>
